from setuptools import setup

setup(
    name='semble',
    version='0.0.1',
    description='System which executes a series of operations to a collection of objects.',
    url='http://gitlab.com/mirkovu/semble',
    author='Mirko Vucicevich',
    author_email='https://git.uwaterloo.ca/mvucicevich/semble',
    license='Unlicense',
    packages=['semble'],
    install_requires=[
        'PyYAML',
    ],
    test_suite='nose.collector',
    tests_require=['nose'],
    scripts=[
        'bin/semble/'
    ],
    zip_safe=False
)
