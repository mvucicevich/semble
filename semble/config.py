from .conveyorbelt import Fork, Path
from .filters import generateFilter


def getModule(module):
    module_name = module.split('.')[0]
    module_path = module.split('.')[1:]
    m = (
        locals().get(module_name) or
        globals().get(module_name) or
        __import__(module_name)
    )
    for x in module_path:
        m = getattr(m, x)
    return m


def parsePath(config, global_context=dict()):
    filters = [
        generateFilter(k, v) for k, v in
        config.pop('filters', dict()).items()
    ]
    print(config)
    if not isinstance(config, Path):
        belt = getModule(config.pop('belt'))
        then = config.pop('then', None)
        if then is not None:
            if isinstance(then, list):
                then = parseFork(then, global_context)
            elif isinstance(then, dict):
                then = parsePath(then, global_context)
            config['then'] = then
    return Path(
        belt,
        global_context=global_context,
        filters=filters,
        **config
    )


def parseFork(config, global_context=dict()):
    paths = []
    for path in config:
        paths.append(parsePath(path, global_context))
    return Fork(
        paths,
        global_context=global_context
    )


def parseStages(stages, global_context=dict()):
    return [
        parseFork(x, global_context)
        for x in stages
    ]
