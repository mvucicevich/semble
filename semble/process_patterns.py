"""Importable useful patterns for belts / assemblages"""


def reactive(self):
    for obj in self.collection:
        proc = self.process_single(obj)
        if proc is not None:
            self.collection.append(proc)
        self.processed.append(obj)


def generative(self, data=None):
    self.processed = self.collection.take_all()
    proc = self.process_single(data)
    if proc is not None:
        self.collection.append(proc)
