import importlib
from .assemblage import Assemblage
from .conveyorbelt import Fork, Path


class AssemblyLine:
    def __init__(self,
                 stages,
                 *,
                 global_context=dict(),
                 initial_collection=None,
                ):

        if isinstance(initial_collection, Assemblage):
            self.collection = initial_collection.take_all()
        # Likely this should be made to take any iterable
        elif isinstance(initial_collection, (list, tuple, set)):
            self.collection = Assemblage(initial_collection)
        elif initial_collection is None:
            self.collection = Assemblage([])
        self.global_context = global_context
        self.stages = stages

    def setup(self):
        pass

    def run(self):
        self.setup()
        for stage in self.stages:
            self.collection.merge(stage.resolve(self.collection))
        self.teardown()

    def teardown(self):
        pass
