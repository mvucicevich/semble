import unittest
import random
from semble import (
    Assemblage,
    ConveyorBelt,
    Option,
    Path,
    Fork
)

class Ob:
    r = range(1, 100000)

    def __init__(self, *, num=None, lst=None):
        self.num = num if num is not None else random.choice(self.r)

    def tag(self, key, val):
        setattr(self, key, val)
        return self


class SimpleCB(ConveyorBelt):
    opt = Option()
    coerced_opt = Option(coerce=str)
    default_opt = Option(default="hi")

    def process_single(self, obj):
        setattr(obj, 'opt_processed', self.option('opt', obj))
        setattr(obj, 'processed', True)
        return obj


def ordered_assemblage(length, on_add=list()):
    return Assemblage([Ob(num=x) for x in range(length)], on_add=on_add)


def ordered_assemblage_with_addfn(length):
        return ordered_assemblage(
            length,
            on_add=[lambda x: x.tag('title', 'Object {}'.format(x.num))]
        )


class TestAssemblage(unittest.TestCase):
    def test_create(self):
        x = Assemblage([Ob()])
        self.assertIsNotNone(next(x))

    def test_iter(self):
        x = ordered_assemblage(10)
        self.assertEqual(len(x), 10)

        for i, j in enumerate(sorted(x, key=lambda y: y.num)):
            self.assertEqual(i, j.num)

        self.assertEqual(len(x), 0)

    def test_getkey(self):
        x = ordered_assemblage(10)
        self.assertEqual(sorted(x.getkey('num')), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
        self.assertEqual(len(x), 10)

        self.assertEqual(
            x.getkey('num', sort_key=lambda x: -1 * x.num),
            (9, 8, 7, 6, 5, 4, 3, 2, 1, 0)
        )


    def test_on_add(self):
        x = ordered_assemblage(
            10,
            on_add=[lambda x: x.tag('title', 'Object {}'.format(x.num))]
        )

        for i, j in enumerate(sorted(x, key=lambda y: y.num)):
            self.assertEqual('Object {}'.format(i), j.title)

    def test_merge(self):
        x = ordered_assemblage_with_addfn(10)

        new = x.spawn()
        new.append(Ob(num=10))

        self.assertEqual(len(new), 1)
        self.assertEqual(next(new).num, 10)

        new.append(Ob(num=10))

        x.merge(new)

        self.assertEqual(len(x), 11)

        y = Assemblage([])

        with self.assertRaises(Exception):
            x.merge(y)

    def test_split(self):
        x = ordered_assemblage_with_addfn(10)

        y = x.split(lambda x: x.num >= 5)

        self.assertEqual(len(y), 5)


class TestConveyorBelt(unittest.TestCase):
    def test_create(self):
        x = ordered_assemblage_with_addfn(10)
        cb = SimpleCB(x)
        cb.run()

        self.assertEqual(len(x), 10)
        for i in x:
            self.assertTrue(i.processed)

    def test_destroy(self):
        x = ordered_assemblage_with_addfn(10)
        cb = SimpleCB(x, destroy=True)
        cb.run()

        self.assertEqual(len(x), 0)

    def test_options(self):
        x = ordered_assemblage_with_addfn(1)
        cb = SimpleCB(
            x,
            opt='T: {title}'
        )
        x.merge(cb.run())
        self.assertEqual(next(x).opt_processed, 'T: Object 0')

    def test_path(self):
        x = ordered_assemblage_with_addfn(10)
        cb = SimpleCB(
            x,
            then=Path(SimpleCB, filters=[lambda x: x.num >= 5], destroy=True)
        )
        cb.run()
        self.assertEqual(len(x), 5)

        cb = SimpleCB(
            x,
            then=Path(SimpleCB)
        )
        cb.run()
        self.assertEqual(len(x), 5)

        cb = SimpleCB(
            x,
            then=Path(SimpleCB, destroy=True)
        )
        cb.run()
        self.assertEqual(len(x), 0)

    def test_fork(self):
        x = ordered_assemblage_with_addfn(10)
        f = Fork([
            Path(SimpleCB, **{'filters': [lambda x: int(x.num) >= 7], 'opt': 'BIG'}),
            Path(SimpleCB, **{'filters': [lambda x: int(x.num) >= 7], 'opt': 'NA'}),
            Path(SimpleCB, **{'filters': [lambda x: int(x.num) <= 3], 'opt': 'SMALL'}),
            Path(SimpleCB, **{'opt': 'MEDIUM'}),
        ])
        x = f.resolve(x).getkey('opt_processed')
        self.assertEqual(len(x), 10)
        self.assertEqual(len([i for i in x if i == 'BIG']), 3)
        self.assertEqual(len([i for i in x if i == 'NA']), 0)
        self.assertEqual(len([i for i in x if i == 'SMALL']), 4)
        self.assertEqual(len([i for i in x if i == 'MEDIUM']), 3)


if __name__ == '__main__':
    unittest.main()
