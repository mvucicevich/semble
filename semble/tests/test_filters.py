from semble.filters import generateFilter, operator_library
import unittest


# Make this a bit less painful
def ol(key, x, tst):
    return operator_library[key](x, tst)


class Ob(object):
    pass


class TestFilters(unittest.TestCase):
    def test_operator_library(self):
        self.assertTrue(ol('gt', 20, 10))
        self.assertTrue(ol('gte', 10, 10))
        self.assertTrue(ol('lt', 5, 10))
        self.assertTrue(ol('lte', 10, 10))
        self.assertTrue(ol('eq', 10, 10))
        self.assertTrue(ol('is', True, True))
        self.assertTrue(ol('contains', 'a', 'abc'))
        self.assertTrue(ol('icontains', 'A', 'abc'))

    def test_generate_filter(self):
        ob = Ob()
        ob.num = 10

        totest = {
            'num__gt': 5,
            'num__lte': 10,
        }

        for k, v in totest.items():
            self.assertTrue(generateFilter(k, v)(ob))
