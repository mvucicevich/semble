import unittest
from unittest.mock import patch
from semble.defaults import *
from semble import Assemblage, Path
from io import StringIO


class TestDefaults(unittest.TestCase):
    def test_createobject_setprops(self):
        x = Assemblage([])
        p = Path(
            CreateObject,
            props={
                'stage1': 'Done'
            },
            then=Path(
                SetProps,
                props={
                    'stage2': 'Done'
                }
            )
        )
        x.merge(p.resolve(x))

        self.assertEqual(len(x), 1)
        self.assertEqual(x.inspect[0].stage1, 'Done')
        self.assertEqual(x.inspect[0].stage2, 'Done')

        p = Path(
            CreateObject,
            destroy=True,
            from_collection=True,
            props={
                'stage3': '{stage1}'
            },
        )
        x.merge(p.resolve(x))

        self.assertEqual(len(x), 1)
        self.assertEqual(x.inspect[0].stage3, 'Done')

    def test_runpythoncode(self):
        x = Assemblage([])

        p = Path(
            CreateObject,
            props={
                'stage1': 'Done'
            },
            then=Path(
                RunPythonCode,
                code='''
somevar = 800*2
obj.stage2 = somevar
return obj
                ''',
                then=Path(
                    RunPythonCode,
                    extra_context={
                        'variable': '{stage2}'
                    },
                    code='''
obj.stage3 = int(variable)
return obj
                    '''
                )
            )
        )
        x.merge(p.resolve(x))

        self.assertEqual(len(x), 1)
        self.assertEqual(x.inspect[0].stage2, x.inspect[0].stage3)

    @patch('sys.stdin', StringIO('semble\nsemble\nsemble'))
    @patch('sys.stdout', StringIO())
    def test_stdin_stdout(self):
        x = Assemblage([])
        p = Path(
            ReadStdin,
        )
        x = p.resolve(x)
        self.assertEqual(
            x.inspect[0].content,
            'semble\nsemble\nsemble'
        )
        p = Path(
            WriteStdout,
        )
        p.resolve(x)
        sys.stdout.seek(0)
        self.assertEqual(
            sys.stdout.read(),
            'semble\nsemble\nsemble\n'
        )

    @patch('sys.stdin', StringIO('semble\nsemble\nsemble'))
    @patch('sys.stdout', StringIO())
    def test_stdin_stdout_multi(self):
        x = Assemblage([])
        p = Path(
            ReadStdin,
            split_lines=True
        )
        x = p.resolve(x)
        self.assertEqual(
            x.inspect[0].content,
            'semble'
        )
        p = Path(
            WriteStdout,
        )
        p.resolve(x)
        sys.stdout.seek(0)
        self.assertEqual(
            sys.stdout.read(),
            'semble\nsemble\nsemble\n'
        )
