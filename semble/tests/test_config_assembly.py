import unittest
import copy
from semble.config import parseStages, parsePath, parseFork
from semble.assemblage import Assemblage
from semble.assemblyline import AssemblyLine

config = {
    'global': {"example": {'value': 1000}},
    'stages': [
        [
            {
                'belt': 'semble.defaults.CreateObject',
                'props': {'num': 10},
                'then': {
                    'belt': 'semble.defaults.SetProps',
                    'props': {'obj': 1}
                }
            },
            {
                'belt': 'semble.defaults.CreateObject',
                'props': {'num': 20},
                'then': {
                    'belt': 'semble.defaults.SetProps',
                    'props': {'obj': 2}
                }
            }
        ],
        [
            {
                'belt': 'semble.defaults.SetProps',
                'props': {'round2': True},
                'then': [
                    {
                        'belt': 'semble.defaults.SetProps',
                        'props': {'text_num': "Text: {num}"},
                        'filters': {
                            'num__eq': 10
                        }
                    },
                    {
                        'belt': 'semble.defaults.SetProps',
                        'props': {'other_num': "Other: {num}"},
                        'filters': {
                            'num__eq': 20
                        }
                    },
                ]
            }

        ],
        [
            {
                'belt': 'semble.defaults.SetProps',
                'props': {'global': '{global_context[example][value]}'}
            }
        ]
    ]
}


class TestConfig(unittest.TestCase):
    def test_path_config(self):
        path = copy.deepcopy(config)['stages'][0][0]
        x = Assemblage([])
        x = parsePath(path).resolve(x)
        self.assertEqual(next(x).obj, 1)

    def test_fork_config(self):
        print(config)
        fork = copy.deepcopy(config)['stages'][0]
        x = Assemblage([])
        x = parseFork(fork).resolve(x)
        self.assertEqual(sorted(x.getkey('obj')), [1, 2])

    def test_fork_sequence(self):
        fork1 = copy.deepcopy(config)['stages'][0]
        fork2 = copy.deepcopy(config)['stages'][1]
        x = Assemblage([])
        x = parseFork(fork1).resolve(x)
        x = parseFork(fork2).resolve(x)
        self.assertEqual(x.getkey('text_num'), ('Text: 10',))
        self.assertEqual(x.getkey('other_num'), ('Other: 20',))

    def test_stages(self):
        x = Assemblage([])
        conf = copy.deepcopy(config)
        global_context = conf.pop('global')
        stages = parseStages(conf['stages'], global_context=global_context)
        for stage in stages:
            x.merge(stage.resolve(x))
        self.assertEqual(x.getkey('text_num'), ('Text: 10',))
        self.assertEqual(x.getkey('other_num'), ('Other: 20',))
        self.assertEqual(x.getkey('global'), ('1000', '1000'))


class TestAssemblyLine(unittest.TestCase):
    def test_basic_assemblyLine(self):
        conf = copy.deepcopy(config)
        global_context = conf.pop('global')
        stages = parseStages(conf['stages'], global_context=global_context)
        x = AssemblyLine(stages, global_context=global_context)
        x.run()
        self.assertEqual(x.collection.getkey('text_num'), ('Text: 10',))
        self.assertEqual(x.collection.getkey('other_num'), ('Other: 20',))
        self.assertEqual(x.collection.getkey('global'), ('1000', '1000'))
