import unittest
import os
import shutil
from semble import fs
from semble import defaults
from semble.conveyorbelt import Path
from semble.assemblage import Assemblage


class Ob:
    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)


class TestFileLoading(unittest.TestCase):
    def setUp(self):
        os.makedirs('__testing__/source/textfiles/')
        os.mkdir('__testing__/source/othertextfiles/')

        with open('__testing__/source/textfiles/test1.txt', 'w') as f:
            f.write("Testing Test 1")
        with open('__testing__/source/textfiles/test2.txt', 'w') as f:
            f.write("Testing Test 2")
        with open('__testing__/source/othertextfiles/test1.txt', 'w') as f:
            f.write("Other text lives here")
        with open('__testing__/source/othertextfiles/test2.md', 'w') as f:
            f.write("Markdown File")

    def tearDown(self):
        shutil.rmtree('__testing__')

    def test_load_files(self):
        x = Assemblage([])
        p = Path(
            fs.FileLoader,
            source_base_dir='__testing__/source/',
            source='textfiles/',
            extensions=['txt']
        )
        x = p.resolve(x)
        self.assertEqual(len(x), 2)
        self.assertEqual(set(x.getkey('filename')), set(('test1', 'test2')))

    def test_load_files_from_collection(self):
        x = Assemblage([Ob(folder='textfiles'), Ob(folder='othertextfiles')])
        p = Path(
            fs.FileLoader,
            source="{folder}/",
            source_base_dir='__testing__/source/',
            from_collection=True,
            extensions=['txt'],
            destroy=True
        )
        x = p.resolve(x)
        self.assertEqual(len(x), 3)
        self.assertEqual(set(x.getkey('filename')), set(('test1', 'test2')))
