from .assemblage import Assemblage


class Path:
    def __init__(self, belt, *, filters=list(), global_context=dict(), **kwargs):
        self.belt = belt
        self.belt_conf = kwargs
        self.filters = filters
        self.global_context = global_context

    def resolve(self, collection):
        filtered = collection.split(
            lambda x: all([f(x) for f in self.filters])
        )
        belt = self.belt(filtered, global_context=self.global_context, **self.belt_conf)
        return belt.run()


class Fork:
    def __init__(self, paths, global_context=dict()):
        self.paths = paths

    def resolve(self, collection):
        # Not async yet, but separate things from the collection
        output = collection.spawn()
        for path in self.paths:
            output.merge(path.resolve(collection))
        return output


class BasicObject:
    def __init__(self):
        pass


class Option:
    def __init__(self, *, coerce=None, default=None, required=False):
        self.coersion_fn = coerce
        self.default = default
        self.required = required

    def coerce(self, data):
        if data is None:
            if self.required:
                raise Exception("Missing required option")
            return self.default

        if self.coersion_fn:
            try:
                data = self.coersion_fn(data)
            except Exception as e:
                raise Warning("Could not coerce data, returning default. Error: {}".format(e))
                return default
        return data


class ConveyorBeltSetup(type):
    def __new__(cls, name, sup, attrs):
        options = {}
        for key, value in list(attrs.items()):
            if isinstance(value, Option):
                options[key] = attrs.pop(key)
        attrs['_options_setup'] = options

        # Generate the new Object Class
        new_class = super().__new__(cls, name, sup, attrs)

        # Climb through the parent classes (if this inherits)
        for base in reversed(new_class.__mro__):
            if hasattr(base, '_options_setup'):
                options.update(base._options_setup)

            for attr, value in base.__dict__.items():
                if value is None and attr in options:
                    options.pop(attr)

        new_class._options_setup = options
        for key, option in options.items():
            privkey = '__{}'.format(key)
            opkey = '__op_{}'.format(key)
            setattr(
                new_class,
                opkey,
                property(
                    lambda self, pk=privkey: self.__dict__.get(pk),
                    lambda self, x, pk=privkey, option=option: setattr(self, pk, option.coerce(x))
                )
            )
        return new_class


class ConveyorBelt(metaclass=ConveyorBeltSetup):
    runtime = 'PRAT'
    props = dict()
    destroy = False
    def_obj = BasicObject

    def __init__(self,
                 asmblg,
                 *,
                 destroy=None,
                 props=None,
                 def_obj=None,
                 global_context=dict(),
                 then=None,
                 **kwargs,
                ):
        self.destroy = destroy if destroy is not None else self.destroy
        self.props = props if props is not None else self.props
        self.def_obj = def_obj if def_obj is not None else self.def_obj
        self.collection = asmblg
        self.processed = asmblg.spawn()
        self.global_context = global_context
        self.then = then
        # Fill out options...
        for key in self.list_options():
            setattr(self, '__op_{}'.format(key), kwargs.get(key, None))
        self.runtime_commands = {
            'P': self.process,
            'R': self.remerge_processed,
            'A': self.apply_props,
            'T': self.then_resolve,
        }


    def option(self, key, obj=None):
        return self.objfmt(getattr(self, '__op_{}'.format(key)), obj)

    def list_options(self):
        return self._options_setup.keys()

    def run(self):
        for r in self.runtime:
            self.runtime_commands[r]()
        return self.collection

    def apply_props(self):
        self.collection.apply(self.set_props)

    def set_props(self, obj):
        for key, val in self.props.items():
            # need to solve global...
            setattr(obj, key, self.objfmt(val, obj))

    def then_resolve(self):
        if self.then:
            self.collection.merge(self.then.resolve(self.collection))

    def process(self):
        for obj in self.collection:
            proc = self.process_single(obj)
            if proc is not None:
                self.processed.append(proc)


    def remerge_processed(self):
        """If destroy argument is not True, merge objects used in "process" back into the collection"""
        if not self.destroy:
            self.collection.merge(self.processed)

    def process_single(self, obj):
        raise NotImplementedError(
            'do_process not implemented for {}. Should return object or None'
            .format(self.__class__.__name__)
        )

    def objfmt(self, target, obj):
        """Formats (using .format) any strings found within target dict or list with globals and object properties"""
        if isinstance(obj, dict):
            d = obj
        elif obj == None:
            d = {}
        elif isinstance(obj, object):
            d = obj.__dict__

        if isinstance(target, str):
            return target.format(global_context=self.global_context, **d)
        elif isinstance(target, list):
            return [self.objfmt(x, obj) for x in target]
        elif isinstance(target, dict):
            return {k: self.objfmt(x, obj) for k, x in target.items()}
        else:
            return target
