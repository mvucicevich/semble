from functools import reduce

class Object:
    def __init__(self):
        pass


class Assemblage:
    def __init__(self, object_list, *, on_add=list()):
        self._on_add = tuple(on_add)
        self._objects = set()
        for obj in object_list:
            self.append(obj)

    def append(self, obj, *, avoid_merge=False):
        if not isinstance(obj, object):
            raise ValueError('Cannot append non-object to assemblage')
        if isinstance(obj, Assemblage):
            if not avoid_merge:
                self.merge(obj)
        else:
            for x in self._on_add:
                obj = x(obj)
                if obj is None:
                    break
            if obj is not None:
                self._objects.add(obj)

    def apply(self, fn):
        for x in self._objects:
            x = fn(x)

    def spawn(self, *, object_list=list()):
        return Assemblage(object_list, on_add=(x for x in self._on_add))

    def take_all(self):
        return self.spawn(object_list=[x for x in self])

    def split(self, fn):
        filtered = set(filter(fn, self._objects))
        self._objects = self._objects - filtered
        return self.spawn(object_list=filtered)

    def merge(self, asmblg):
        if self._on_add != asmblg._on_add:
            raise Exception('Assemblage cannot merge: incompatable on_add list')
        self._objects = self._objects.union(asmblg._objects)

    def __iter__(self):
        for x in range(len(self._objects)):
            yield self._objects.pop()

    def __next__(self):
        return self._objects.pop()

    def __len__(self):
        return len(self._objects)

    @property
    def inspect(self):
        return tuple(self._objects)

    def getkey(self, key, *, sort_key=None):
        if sort_key is not None:
            viewport = sorted(
                [y for y in self._objects if hasattr(y, key)],
                key=sort_key
            )
        else:
            viewport = [y for y in self._objects if hasattr(y, key)]
        return tuple([getattr(x, key) for x in viewport])



