from .conveyorbelt import ConveyorBelt as CB, Option
from . import process_patterns
import os
import re


class FileLoader(CB):
    __help__ = """This just loads files -- it is the parent class for other file loaders"""
    source = Option(coerce=str, required=True)
    from_collection = Option(default=False)
    source_base_dir = Option(coerce=str, default='./source')
    depth = Option(coerce=int, default=0)
    extensions = Option(coerce=lambda x: [str(y) for y in x], default=[])
    regex = Option(coerce=str, default='')
    runtime = 'PATR'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.regex = getattr(self, '__op_regex')
        self.extensions = getattr(self, '__op_extensions')
        if self.regex == '' and self.extensions == []:
            raise Exception(
                '{} needs either <extensions> list or <regex> str'
                .format(self.__class__.__name__)
            )

    def process(self):
        if not self.option('from_collection'):
            process_patterns.generative(self)
        else:
            process_patterns.reactive(self)

    def process_single(self, obj):
        files = self.get_files(obj)
        new_collection = self.collection.spawn(object_list=files)
        return new_collection

    def make_obj(self, root, f):
        filepath, filename = os.path.split(f)
        name, ext = os.path.splitext(filename)
        ext = ext.strip('.')
        if (
            (self.regex != '' and re.match(self.regex, ext) is not None) or
            (self.extensions != [] and ext in self.extensions)
        ):
            new_obj = self.def_obj()
            new_obj.filename = name
            new_obj.source = os.path.join(root, f)
            new_obj.extension = ext.strip('.')
            return new_obj
        else:
            return None

    def get_files(self, obj):
        collected_files = []
        source = os.path.join(
            self.option('source_base_dir', obj),
            self.option('source', obj)
        )
        if not source.endswith('/'):
            if not os.path.isfile(source):
                raise Exception(
                    'Invalid source (not a file). did you mean "{}/"'
                    .format(source)
                )
            n = self.make_obj('', source)
            if n is not None:
                collected_files.append(n)
            return collected_files
        else:
            if not os.path.isdir(source):
                raise Exception('Invalid source directory')
        for root, dirs, files in os.walk(source):
            for f in files:
                n = self.make_obj(root, f)
                if n is not None:
                    collected_files.append(n)
        return collected_files
