from semble import Option, ConveyorBelt as CB, process_patterns
import ast
import sys


# Basic functionality for object manip
class CreateObject(CB):
    __help__ = (
        "Creates objects. By default (if from_collection is False) "
        "only a single object will be created, and take props from "
        "the belt configuration.\n"
        "If from_collection is set to True, a new object will be "
        "created for each object passed into this belt. prop formatting "
        "will pull variables from the original object onto the new one."
    )
    from_collection = Option(default=False)
    runtime = 'PTR'

    def process(self):
        if self.option('from_collection'):
            process_patterns.reactive(self)
        else:
            process_patterns.generative(self)

    def process_single(self, obj):
        ob = self.def_obj()
        for key, val in self.props.items():
            # need to solve global...
            setattr(ob, key, self.objfmt(val, obj))
        return ob

    def apply_props(self, obj):
        pass


class SetProps(CB):
    runtime = 'AT'

    def process(self):
        pass


# Arbitrary Execution
class RunPythonCode(CB):
    """Runs Multiline Python directly. VERY unsafe.

    the code option does not format with objfmt, pass
    any variables which require compiling to extraContext dict


    code should end with 'return obj'
    """

    code = Option(required=True)
    extra_context = Option(coerce=dict, default=dict())

    def process_single(self, obj):
        block = ast.parse(self.option('code'), mode='exec')
        last = ast.Expression(block.body.pop().value)

        local_vars = {'obj': obj, 'self': self}
        local_vars.update(self.option('extra_context', obj))
        print(local_vars)
        exec(
            compile(block, '<string>', mode='exec'),
            self.global_context,
            local_vars,
        )
        return eval(
            compile(last, '<string>', mode='eval'),
            self.global_context,
            local_vars,
        )


# Stdin / stdout stuff...
class ReadStdin(CB):
    """Reads Stdin"""
    runtime = 'PATR'
    to_prop = Option(default='content')
    split_lines = Option(default=False)

    def process(self):
        self.processed = self.collection.take_all()
        if not self.option('split_lines'):
            data = sys.stdin.read()
            self.collection.append(self.process_single(data))
        else:
            data = [line.rstrip() for line in sys.stdin.readlines()]
            for x in data:
                self.collection.append(self.process_single(x))


    def process_single(self, data):
        obj = self.def_obj()
        setattr(obj, self.option('to_prop'), data)
        return obj


class WriteStdout(CB):
    destroy = True
    from_prop = Option(default='content')

    def process_single(self, obj):
        sys.stdout.write(getattr(obj, self.option('from_prop', obj)))
        sys.stdout.write('\n')
        return obj
