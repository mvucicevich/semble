"""===========================
Semble Assembly Line System
===========================

Semble is a tool intended to allow users to define sequences of arbitrary
operations on a collection of arbitrary objects. Operations can be likened to
materials moving down a series of conveyor belts, with operations occuring
along different belts.


The simplest, most common use case for this tool is generating static
documents or websites from a collection of raw source materials (such as
plaintext files, databases, and urls)


Core Concepts
===============

Setting up a production line, one will start with an initial collection of
objects (or an empty collectio) and pass that collection into a Path or Fork.

Paths and Forks are wrappers around ConveyorBelt objects -- a Path being a
pairing of a ConveyorBelt with optional filtering, a Fork being a collection
of simultanous Paths.

Each ConveyorBelt takes some configuration Options and will run a process (
usually operating on each item in the collection it is passed.) After the
processing is complete, processed objects can be re-merged into the collection
 and continue on to the next Path / Fork.


The following diagram shows a simple example of a process:


Collections (Assemblage Object)
-------------------------------


ConveyorBelts
-------------------------------



Paths / Forks
-------------------------------
"""
from .assemblage import Assemblage
from .conveyorbelt import ConveyorBelt, Path, Fork, Option
