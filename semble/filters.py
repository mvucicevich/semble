from functools import partial


operator_library = {
    'gt': lambda v, tst: v > tst,
    'lt': lambda v, tst: v < tst,
    'gte': lambda v, tst: v >= tst,
    'lte': lambda v, tst: v <= tst,
    'eq': lambda v, tst: v == tst,
    'is': lambda v, tst: v is tst,
    'contains': lambda v, tst: v in tst,
    'icontains': lambda v, tst: v.lower() in tst.lower(),
}


def runFuncIfAttr(attr, func, arg, obj):
    if hasattr(obj, attr):
        return func(getattr(obj, attr), arg)
    else:
        return lambda v, y: False


def generateFilter(key, val):
    parts = key.split('__')
    key = parts[0]
    operator = parts[1:]
    if len(operator) > 1:
        raise Exception("operator too long. Use operators such as __gt")
    elif len(operator) == 0:
        raise Exception("No operator found")

    operator = operator_library.get(operator[-1])
    return partial(runFuncIfAttr, key, operator, val)
